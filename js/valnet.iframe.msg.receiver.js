/*
 * valnet.iframe.msg.receiver.js (valnet.iframe.msg)
 * author: Krozz Wu
 * version: 1.0.0
 * desc.: bridge between iframe window and current document window
 * note: valnet.iframe.msg.receiver.js has to loaded within the desired iframe window,
 *       valnet.iframe.msg.sender.js will send message to desired iframe,
 *       when message value matched, action within desired iframe will be invoke.
 */

console.log('listening...');

var video = document.querySelectorAll('video')[0];
var getParentMsg = function(e) {
    if (e.data === 'play') {
        video.play();
    }
};
window.addEventListener("message", getParentMsg, false);

